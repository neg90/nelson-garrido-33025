# Nelson Garrido | Web cursada coderhouse

Guía de instalación del proyecto para desarrollador.

## Instalación 

Clone el proyecto dentro de la carpeta www de su servidor web local.
```bash
git clone https://gitlab.com/neg90/nelson-garrido-33025 web-coder
```
Por consola ingrese a la carpeta del repositorio
```bash
cd /web-coder
```
Instale las dependecias NPM para desarrollo 
```bash
npm install
```

## Autor
Nelson Garrido