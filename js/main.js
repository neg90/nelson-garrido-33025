const btn_sidenav_abrir = document.getElementById('btn-abrir-menu'),
    btn_sidenav_cerrar = document.getElementById('btn-cerrar-sidenav'),
    sidenav = document.getElementById('sidenav');

btn_sidenav_abrir.addEventListener('click', abrir_sidenav);
function abrir_sidenav() {
    sidenav.style.width = `320px`;
}

btn_sidenav_cerrar.addEventListener('click', cerrar_sidenav);
function cerrar_sidenav() {
    sidenav.style.width = `0px`;
}